package com.github.ghsea.dbtracer.xml;

import com.github.ghsea.dbtracer.config.DbTraceConfigException;

/**
 * TODO 细化：
 * 1.表未配置
 * 2.字段未配置
 * 3.配置的字段在表中不存在
 * @author GuHai
 * 2017-8-11下午9:50:39
 */
public class XmlDbTraceConfigException extends DbTraceConfigException {

	private static final long serialVersionUID = 8601975631294049764L;

	public XmlDbTraceConfigException(String cause) {
		super(cause);
	}
}
