package com.github.ghsea.dbtracer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.ghsea.dbtracer.config.DbTraceConfigException;
import com.github.ghsea.dbtracer.xml.TableConfiguration;
import com.github.ghsea.dbtracer.xml.XmlParser;

public class Bootstrap {

	private static Map<String, TableConfiguration> tableConfig;

	private static boolean init = false;

	private static Object lock = new byte[0];

	private static Logger logger = LoggerFactory.getLogger(Bootstrap.class);

	private static String tableConfigXmlFile;

	/**
	 * 
	 * @Description: TODO
	 * @param fileName
	 *            全局配置文件的相对路径
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static void setUp(String fileName) throws FileNotFoundException, IOException {
		if (fileName == null) {
			throw new IllegalArgumentException("fileName is null");
		}

		synchronized (lock) {
			if (init) {
				logger.error("DbTracerConfig has been already setUp.");
				return;
			}

			Properties config = loadProperties(fileName);
			String tblConfFile = config.getProperty("table.config");
			tableConfigXmlFile = tblConfFile;
			if (StringUtils.isBlank(tblConfFile)) {
				throw new DbTraceConfigException(
						"Property named 'table.config' can not be null nor blank.Please check the config file:"
								+ fileName);
			}
			initTableConfig(tblConfFile);

			init = true;
		}
	}

	private static Properties loadProperties(String fileName) throws IOException {
		FileReader configFileReader = null;
		try {
			String rootClassPath = Class.class.getResource("/").getPath();
			String path = rootClassPath + fileName;

			logger.info("Load config file from" + path);

			File file = new File(path);
			Properties config = new Properties();
			configFileReader = new FileReader(file);
			config.load(configFileReader);
			return config;
		} finally {
			if (configFileReader != null) {
				configFileReader.close();
			}
		}
	}

	private static void initTableConfig(String tblConfFile) {
		XmlParser parse = new XmlParser(tblConfFile);
		tableConfig = parse.parse();
	}

	public static Map<String, TableConfiguration> getTableConfiguration() {
		checkState();
		return tableConfig;
	}

	public static String getTableConfigFile() {
		return tableConfigXmlFile;
	}

	private static void checkState() {
		if (!init) {
			throw new IllegalStateException(
					"Dbtracer has not been setUp.Please com.github.ghsea.dbtracer.Bootstrap.setUp() to config first .");
		}
	}

}
