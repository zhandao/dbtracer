package com.github.ghsea.dbtracer.interceptor;

import java.lang.reflect.Method;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.reflect.MethodSignature;

public class EntryIntercptor {

	public Object doAround(JoinPoint jp) throws Exception {
		try {
			MethodSignature sig = (MethodSignature) jp.getSignature();
			Method method = sig.getMethod();
			DbTraceEntry traceEntry = method.getAnnotation(DbTraceEntry.class);
			if (null != traceEntry) {
				DbTracerContext.setNeedTrace(true);
				DbTracerContext.setBizName(traceEntry.bizName());
			}

			Object ret = method.invoke(jp.getTarget(), jp.getArgs());
			return ret;
		} catch (Exception ex) {
			throw ex;
		} finally {
			DbTracerContext.clean();
		}
	}

}
