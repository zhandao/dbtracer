//package com.github.ghsea.dbtracer.sample.customize;
//
//import java.sql.Connection;
//import java.sql.SQLException;
//
//import javax.sql.DataSource;
//
//import org.springframework.jdbc.datasource.DataSourceUtils;
//
//import com.github.ghsea.dbtracer.db.ConnectionProxy;
//import com.github.ghsea.dbtracer.sample.service.SpringUtil;
//
//public class SampleConnectionProxy implements ConnectionProxy {
//
//	private DataSource ds;
//
//	public SampleConnectionProxy() {
//		this.ds = (DataSource) SpringUtil.getBean("dataSource");
//	}
//
//	public Connection getConnection() throws SQLException {
//		return ds.getConnection();
//	}
//
//	public void releaseConnection(Connection conn) {
//		DataSourceUtils.releaseConnection(conn, ds);
//	}
//
//}
