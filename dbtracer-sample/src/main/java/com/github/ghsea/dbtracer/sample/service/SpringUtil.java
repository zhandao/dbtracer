package com.github.ghsea.dbtracer.sample.service;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringUtil {
	private static AbstractApplicationContext ctx;

	private SpringUtil() {

	}

	static {
		ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
		ctx.registerShutdownHook();
	}

	public static <T> T getBean(Class<T> type) {
		return ctx.getBean(type);
	}

	public static Object getBean(String name) {
		return ctx.getBean(name);
	}

	public static void main(String[] args) {
		System.out.println("It's just for test");
	}
}
