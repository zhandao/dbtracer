create database if not exists dbtracer  DEFAULT CHARACTER SET utf8;
use dbtracer;
create table test_module(
id  bigint auto_increment,
name nvarchar(20) not null comment '',
address nvarchar(20)  not null comment '',
group_id int not null comment '',
create_time datetime not null comment '',
is_delete tinyint not null comment '',
primary key(id)
)ENGINE=InnoDB;

##插入规则表测试数据
insert into test_module(name,address,group_id,create_time,is_delete)value('ghsea','hubei wuhan',1,now(),0);